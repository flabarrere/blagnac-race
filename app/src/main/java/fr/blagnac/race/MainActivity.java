package fr.blagnac.race;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap gMap;
    private LocationManager locationManager;

    private TimerRace timerRace;

    private TextView cibleTv;
    private Location cibleLoc;

    private Marker myMarker;
    private Marker cibleMarker;


    private String adresse_serveur;
    private String monNom;
    private Boolean courseFinie=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && this.checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && this.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            return;

        gMap.setMyLocationEnabled(false); // n'affiche pas le rond bleu de localisation
        gMap.getUiSettings().setZoomControlsEnabled(true); // affiche les boutons de zoom
        gMap.getUiSettings().setZoomGesturesEnabled(true); // autorise le zoom tactile
        gMap.getUiSettings().setCompassEnabled(false); // n'affiche pas le compas
        gMap.getUiSettings().setMyLocationButtonEnabled(true); // affiche le bouton de localisation

        String cibleDesc = "Inconnue"; // description de la cible
        double cibleLat = 43.6489983; // coordonnées (latitude, longitude)
        double cibleLon = 1.3749359; // de l’IUT de Blagnac
        cibleLoc = new Location("Cible") ;
        cibleLoc.setLatitude(cibleLat);
        cibleLoc.setLongitude(cibleLon);
        cibleTv = (TextView)findViewById(R.id.tv);
        String cibleTexte = " Cible : " + cibleDesc;
        cibleTv.setText(cibleTexte);

        myMarker = gMap.addMarker(new MarkerOptions()
                .title("Moi")
                .position(new LatLng(0, 0))
                .icon(BitmapDescriptorFactory
                        .fromResource(R.drawable.green_droid))); // Icone Droid vert
        cibleMarker = gMap.addMarker( new MarkerOptions()
                .title("Cible")
                .snippet(cibleDesc)
                .position(new LatLng(cibleLat,cibleLon))
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE)) ); // Marqueur bleu
        timerRace = new TimerRace(this,86400000, 5000 );

        new RejoindreCourse( this ).show();
    }


    @Override
    public void onLocationChanged(final Location myLoc) {
        // Récupération des cordonnées (latitude, longitude) et création d’un objet
        // myPos de la classe LatLng représentant cette position
        final LatLng myPos = new LatLng(myLoc.getLatitude(), myLoc.getLongitude());
        // Centrage de la carte sur la position GPS obtenue
        gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPos, 15));

        double distance=myLoc.distanceTo(cibleLoc);

        RequeteHTTP rq = new RequeteHTTP(adresse_serveur);
        String raiponce, messageAdversaire;
        try {

            //lecture des messages
            while(!(raiponce = rq.doGET("cmd=getMessage&name="+monNom )).equals("None"))
            {
                if( "perdu".equals(raiponce) ) {
                    courseFinie = true;
                }
                else {
                    Toast.makeText( this, raiponce, Toast.LENGTH_LONG ).show();
                }
            }

            //si course finie
            if( courseFinie )
            {
                raiponce = rq.doGET("cmd=getGagnant");
                this.showAlert("Perdu !!", raiponce + " a gagné");
                desabonnementGPS();
                timerRace.cancel();
                rq.doGET("cmd=removeParticipant&name="+monNom);
                return;
            }

            rq.doGET("cmd=setPosition&name="+monNom+"&lat="+myLoc.getLatitude()+"&lon="+myLoc.getLongitude());

            if (distance < 30.0) {
                raiponce = rq.doGET("cmd=setGoalReached&name="+monNom);
                if (Integer.parseInt(raiponce) == 3){
                    courseFinie = true;
                    this.showAlert("Bravo !!", "Vous avez gagné");
                    messageAdversaire = "perdu";
                    rq.doGET("cmd=removeParticipant&name="+monNom);
                    this.desabonnementGPS();
                    timerRace.cancel();
                }
                else
                {
                    this.showAlert("Bravo !!", "Vous avez atteint la cible "+raiponce);
                    messageAdversaire = monNom + " a atteint la cible " + raiponce;
                    raiponce = rq.doGET("cmd=getGoal&name="+ monNom );

                    String[] cible = raiponce.split(",");
                    System.out.println(Arrays.toString(cible));
                    getCibleTv().setText( "Cible: "+ cible[0] );
                    getCibleLoc().setLatitude( Double.parseDouble(cible[1]) );
                    getCibleLoc().setLongitude( Double.parseDouble(cible[2]) );
                    getCibleMarker().setPosition(new LatLng( Double.parseDouble(cible[1]), Double.parseDouble(cible[2]) ));
                }

                raiponce = rq.doGET("cmd=getParticipants");
                for( String player : raiponce.split(","))
                    rq.doGET("cmd=sendMessage&name="+player+"&msg="+messageAdversaire );
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        myMarker.setPosition(myPos);
        myMarker.setSnippet("à "+ Math.round(distance) + " m de la cible");
        myMarker.showInfoWindow();
    }

    @Override
    public void onStatusChanged(final String provider, final int status, final Bundle extras) {}

    @Override
    public void onProviderEnabled(final String provider) {
        if ("gps".equals(provider)) { // Si le GPS est activé on s'abonne
            abonnementGPS();
        }
    }

    @Override
    public void onProviderDisabled(final String provider) {
        if ("gps".equals(provider)) { // Si le GPS est désactivé on se désabonne
            desabonnementGPS();
        }
    }


    public void abonnementGPS() throws SecurityException {
        // demande de réception de localisations du GPS au minimum toutes les 5000 ms et
        // lorsque la distance entre la dernière localisation reçue est supérieure à 10 m
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this);
    }
    public void desabonnementGPS() throws SecurityException {
        locationManager.removeUpdates(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        // On appelle la méthode pour se désabonner
        desabonnementGPS();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Obtention de la référence du service
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        // Si le GPS est disponible, on s'y abonne
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            abonnementGPS();
        }
    }

    public void showAlert(String titre, String message) {
        AlertDialog.Builder ADBuilder = new AlertDialog.Builder(this);
        ADBuilder.setTitle(titre);
        ADBuilder.setMessage(message);
        ADBuilder.setCancelable(true);
        ADBuilder.setNeutralButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog AD = ADBuilder.create();
        AD.show();
    }

    public String getAdresse_serveur() {
        return adresse_serveur;
    }

    public void setAdresse_serveur(String adresse_serveur) {
        this.adresse_serveur = adresse_serveur;
    }

    public String getnNom() {
        return monNom;
    }

    public void setNom(String monNom) {
        this.monNom = monNom;
    }

    public Boolean isCourseFinie() {
        return courseFinie;
    }

    public void setCourseFinie(Boolean courseFinie) {
        this.courseFinie = courseFinie;
    }

    public Marker getMyMarker() {
        return myMarker;
    }

    public TextView getCibleTv() {
        return cibleTv;
    }

    public Location getCibleLoc() {
        return cibleLoc;
    }

    public Marker getCibleMarker() {
        return cibleMarker;
    }

    public TimerRace getTimerRace() {
        return timerRace;
    }

    public GoogleMap getgMap() {
        return gMap;
    }
}
