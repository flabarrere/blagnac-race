package fr.blagnac.race;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.CountDownTimer;
import android.widget.Toast;

public class TimerRace extends CountDownTimer{
	private final MainActivity mainActivity;
	private HashMap<String,Marker> marqueursAdversaires;

	public TimerRace(MainActivity mainAct, long millisInFuture, long countDownInterval) {
		super(millisInFuture, countDownInterval);
		this.mainActivity=mainAct;
		this.marqueursAdversaires = new HashMap<String,Marker>();
	}

	@Override
	public void onTick(long millisUntilFinished) {
		RequeteHTTP requeteServeur = new RequeteHTTP(mainActivity.getAdresse_serveur());
		try {
			String[] participants = requeteServeur.doGET("cmd=getParticipants").split(",");
			String monNom = mainActivity.getnNom();
			String[] positions;
			Marker currentMarker;
            for( String participant : participants ){
                if( !participant.equals( monNom )){

                    positions = requeteServeur.doGET("cmd=getPosition&name="+participant).split(",");
                    if( !positions[0].equals("None")){
                        if( marqueursAdversaires.containsKey( participant )){
                            currentMarker = marqueursAdversaires.get( participant );
                        }
                        else{
                            currentMarker = mainActivity.getgMap().addMarker(new MarkerOptions()
                                    .title(participant)
                                    .position(new LatLng(0, 0))
                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.red_droid)));
                            marqueursAdversaires.put( participant, currentMarker );
                        }
                        currentMarker.setPosition(new LatLng( Double.parseDouble(positions[0]), Double.parseDouble(positions[1]) ));
                    }
                }
			}

		}
		catch  (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onFinish() {
	}

}
