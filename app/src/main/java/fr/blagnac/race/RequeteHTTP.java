package fr.blagnac.race;

import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

public class RequeteHTTP {
	private String adresse_serveur;

	public RequeteHTTP(String adresse_serveur){
		this.adresse_serveur = adresse_serveur;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
	}

	public String doGET(String parametres) throws MalformedURLException, IOException {

		Log.i("Request", "http://" + adresse_serveur + "/?" + parametres);
		//cr�ation de l'objet URL pour la requ�te
		URL url = new URL("http://" + adresse_serveur + "/?" + parametres );
		//ouverture de la connection et param�trage de la requ�te
		HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
		httpUrlConnection.setReadTimeout(10000 /* milliseconds */);
		httpUrlConnection.setConnectTimeout(15000 /* milliseconds */);
		httpUrlConnection.setRequestMethod("GET");
		httpUrlConnection.setDoInput(true);
		//envoi de la requ�te au serveur
		httpUrlConnection.connect();
		if(httpUrlConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
		{
			//r�cup�ration de la r�ponse
			StringBuilder reponse = new StringBuilder();

			InputStream is = new BufferedInputStream(httpUrlConnection.getInputStream());

			byte[] tabByte = new byte[1];

			while( (is.read(tabByte)!=-1) )
				reponse.append(new String(tabByte));

			Log.i("Response", reponse.toString());

			httpUrlConnection.disconnect();
			return reponse.toString();
		} else {
			httpUrlConnection.disconnect();
			throw new MalformedURLException();
		}
	}

}

