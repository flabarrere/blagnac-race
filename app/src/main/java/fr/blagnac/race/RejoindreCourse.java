package fr.blagnac.race;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Dialog;
import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class RejoindreCourse extends Dialog{
	private final MainActivity mainActivity;

	private static JSONParser parser = new JSONParser();

	public RejoindreCourse(MainActivity mainAct) {
		super(mainAct);
		this.mainActivity=mainAct;
		setContentView(R.layout.connexion_serveur);
		setTitle("Connexion au serveur");
		Button ok = (Button) findViewById(R.id.button_ok);
		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                String adresse_serveur = ((EditText) findViewById(R.id.ed_serveur)).getText().toString();
                String nom = ((EditText) findViewById(R.id.ed_nom)).getText().toString();
                mainActivity.setAdresse_serveur(adresse_serveur);
                mainActivity.setNom(nom);
                mainActivity.getMyMarker().setTitle(nom);
                try {
                    RequeteHTTP requeteServeur = new RequeteHTTP(adresse_serveur);
                    String reponse = requeteServeur.doGET("cmd=isRaceOnProgress");
                    if (reponse.equals("None")) {
                        mainActivity.desabonnementGPS();
                        mainActivity.showAlert("Erreur", "Le serveur n'est pas actif");
                        dismiss();
                    } else if (reponse.equals("False")) {
                        Toast.makeText(mainActivity, "Premier participant \n  Initialisation de la course", Toast.LENGTH_LONG).show();
                        requeteServeur.doGET("cmd=reinitRace");
                    }

                    requeteServeur.doGET("cmd=addParticipant&name=" + nom);
                    reponse = requeteServeur.doGET("cmd=getGoal&name=" + nom);

                    String[] cible = reponse.split(",");
                    System.out.println(Arrays.toString(cible));
                    mainActivity.getCibleTv().setText("Cible: " + cible[0]);
                    mainActivity.getCibleLoc().setLatitude(Double.parseDouble(cible[1]));
                    mainActivity.getCibleLoc().setLongitude(Double.parseDouble(cible[2]));
                    mainActivity.getCibleMarker().setPosition(new LatLng(Double.parseDouble(cible[1]), Double.parseDouble(cible[2])));

                    mainActivity.abonnementGPS();
                    mainActivity.getTimerRace().start();

                } catch (MalformedURLException e) {
                    mainActivity.showAlert("Erreur", "URL invalide");
                } catch (IOException e) {
                    mainActivity.showAlert("Erreur", "Pas d'accés au réseau");
                    dismiss();
                }

                try {
                    URL url = new URL("https://data.toulouse-metropole.fr/explore/dataset/stations-de-tramway/download/?format=json&timezone=Europe/Berlin");
                    HttpURLConnection connection = null;
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();

                    Iterator iterator = ((JSONArray) parser.parse(new InputStreamReader(connection.getInputStream()))).iterator();
                    JSONObject jsonGroup;
                    while (iterator.hasNext()) {
                        jsonGroup = (JSONObject) iterator.next();
                        jsonGroup = (JSONObject) jsonGroup.get("fields");

                        mainActivity.getgMap().addMarker( new MarkerOptions()
                                .title((String) jsonGroup.get("nom"))
                                .position(new LatLng((double)((JSONArray)jsonGroup.get("geo_point_2d")).get(0),(double)((JSONArray)jsonGroup.get("geo_point_2d")).get(1)))
                                .icon(BitmapDescriptorFactory
                                        .defaultMarker(BitmapDescriptorFactory.HUE_CYAN)) );

                    }
                } catch (Exception e) {
                    System.err.println(e);
                }
                dismiss();
            }
		});
	}
}
