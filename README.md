# Blagnac Race

Vous allez réaliser sur 4 semaines une petite application de course à travers Blagnac mettant en compétition à plusieurs joueurs qui devront chacun atteindre consécutivement 3 lieux de Blagnac. Une application serveur hébergée sur Google App Enginesera utilisée pour localiser les différents joueurs, leur donner des indications sur les lieux à atteindre et synchroniser la course.

Le serveur sera fourni pour les étudiants du PAC1, et réalisé dans le module Python pour les étudiants du PAC2

Le projet sera réalisé en différentes étapes progressives. Vous développerez d’abords une application aux fonctionnalités communes identiques, selon le principe suivant :
 - au démarrage, le serveur en ligne génère au hasard une liste de 3 lieux ‘cibles’ de Blagnac à atteindre.
 - quand un joueur atteint une cible, tous les autres joueurs en sont notifiés, puis on lui indique la prochaine cible à atteindre.
 - la course se termine lorsque qu’un joueur a atteint la dernière cible.
 - à tout instant, un joueur peut localiser sur une carte Google Maps sa position, sa prochaine cible, et la position des adversaires.

 Cette 1ère étape très guidée est plus un tutoriel ayant pour but de créer le ‘coeur’ de l’activité principale de l’application : le déplacement d’un joueur géolocalisé par GPS sur une carte Google Maps, affichant sa position ainsi que celle d’un lieu cible.

 L’étape suivante vous fera vous connecter au serveur en ligne et échanger des messages de synchronisation entre le serveur et les autres participants pour mettre en place la course proprement dite.

 Enfin, la dernière étape, entièrement personnelle, vous permettra de rajouter des fonctionnalités supplémentaires au choix à votre application (réutilisant des notions vues aux TPs précédents ou de nouvelles dont la documentation vous sera fournie).